"use strict";
var path = require("path")
, fs = require("fs")
, streams = require("./streams")
, os = require("os")
, eol = os.EOL || "\n"
, openFiles = [];
var util = require("util");
//close open files on process exit.
process.on("exit", function() {
    openFiles.forEach(function (file) {
        file.end();
    });
});

function basicLayout (loggingEvent, timezoneOffset) {
    var obj = {};
    obj.date = new Date(loggingEvent.startTime).toISOString();
    obj.masterName = loggingEvent.categoryName;
    if(loggingEvent.level["levelStr"] == "AUDIT"){
        try {
            var log = JSON.parse(loggingEvent.data[0]);
            obj.operation = log.operation;
            obj.user = log.user;
            obj.id = log._id?log._id:"";
            obj.timestamp = log.timestamp;
            obj.originalValues = log.originalValues;
            obj.newValues = log.newValues;
            obj.message = log.message?log.message:"";
        } catch (error) {
            obj.log = loggingEvent.data[0]?JSON.stringify(loggingEvent.data[0]).replace(/\\\"/g,""):"";    
        }
    }
    else
        obj.log = loggingEvent.data[0]?JSON.stringify(loggingEvent.data[0]).replace(/\\\"/g,""):"";
    obj.level = loggingEvent.level["levelStr"];
    return util.format(JSON.stringify(obj));
}
/**
 * File Appender writing the logs to a text file. Supports rolling of logs by size.
 *
 * @param file file log messages will be written to
 * @param layout a function that takes a logevent and returns a string
 *   (defaults to basicLayout).
 * @param logSize - the maximum size (in bytes) for a log file,
 *   if not provided then logs won't be rotated.
 * @param numBackups - the number of log files to keep after logSize
 *   has been reached (default 5)
 * @param compress - flag that controls log file compression
 * @param timezoneOffset - optional timezone offset in minutes (default system local)
 */
function fileAppender (file, layout, logSize, numBackups, compress, timezoneOffset) {
    var bytesWritten = 0;
    file = path.normalize(file);
    layout = basicLayout;
    numBackups = numBackups === undefined ? 5 : numBackups;
  //there has to be at least one backup if logSize has been specified
    numBackups = numBackups === 0 ? 1 : numBackups;

    function openTheStream(file, fileSize, numFiles) {
        var stream;
        if (fileSize) {
            stream = new streams.RollingFileStream(
        file,
        fileSize,
        numFiles,
        { "compress": compress }
      );
        } else {
            stream = fs.createWriteStream(
        file,
        { encoding: "utf8",
          mode: parseInt("0644", 8),
          flags: "a" }
      );
        }
        stream.on("error", function (err) {
            console.error("log4js.fileAppender - Writing to file %s, error happened ", file, err);
        });
        return stream;
    }

    var logFile = openTheStream(file, logSize, numBackups);

  // push file to the stack of open handlers
    openFiles.push(logFile);

    return function(loggingEvent) {
        logFile.write(layout(loggingEvent, timezoneOffset) + eol, "utf8");
    };

}

function configure(config, options) {
    var layout;
    if (config.layout) {
    //layout = layouts.layout(config.layout.type, config.layout);
        layout = basicLayout;
    }

    if (options && options.cwd && !config.absolute) {
        config.filename = path.join(options.cwd, config.filename);
    }

    return fileAppender(
    config.filename,
    layout,
    config.maxLogSize,
    config.backups,
    config.compress,
    config.timezoneOffset
  );
}

function shutdown(cb) {
    var completed = 0;
    var error;
    var complete = function(err) {
        error = error || err;
        completed++;
        if (completed >= openFiles.length) {
            cb(error);
        }
    };
    if (!openFiles.length) {
        return cb();
    }
    openFiles.forEach(function(file) {
        if (!file.write(eol, "utf-8")) {
            file.once("drain", function() {
                file.end(complete);
            });
        } else {
            file.end(complete);
        }
    });
}

exports.appender = fileAppender;
exports.configure = configure;
exports.shutdown = shutdown;
